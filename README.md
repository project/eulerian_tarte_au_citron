# Eulerian for Tarte au Citron

Allows to add Eulerian service to [Tarte au Citron](https://www.drupal.org/project/tarte_au_citron) module.

For a full description of the module, visit the [project page](https://www.drupal.org/project/eulerian_tarte_au_citron).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/eulerian_tarte_au_citron).


## Requirements

This module requires the following modules:
* [Eulerian](https://www.drupal.org/project/eulerian)
* [Tarte au Citron](https://www.drupal.org/project/tarte_au_citron)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.
When enabled, the module will prevent the Eulerian script from running.
To run the Eulerian script by default, disable the module and clear caches.

The design of [Tarte au Citron](https://tarteaucitron.io/) library is that the Eulerian script is only triggered when the user has given consent.
However, Eulerian may be exempt from consent (see this [CNIL configuration guide (in French)](https://www.cnil.fr/sites/default/files/atoms/files/eulerian_-_guide_de_parametrage.pdf)).
You can choose to run the Eulerian script without consent when activating the service in the [Tarte au Citron](https://www.drupal.org/project/tarte_au_citron) module configuration page.
