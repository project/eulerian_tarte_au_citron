<?php

declare(strict_types=1);

namespace Drupal\eulerian_tarte_au_citron\Plugin\tarte_au_citron;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tarte_au_citron\ServicePluginBase;

/**
 * An Eulerian service plugin.
 *
 * @TarteAuCitronService(
 *   id = "eulerian-analytics",
 *   title = @Translation("Eulerian Analytics")
 * )
 */
class Eulerian extends ServicePluginBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'need_consent' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getLibraryName(): string {
    return 'eulerian_tarte_au_citron/service.' . ($this->getSetting('need_consent') ? 'tarte_au_citron' : 'eulerian');
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);

    $elements['need_consent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Need consent'),
      '#default_value' => $this->getSetting('need_consent'),
      '#description' => $this->t('If consent is not required, your site tagging will be active.
        You must use Eulerian\'s TCFv2 feature to be GDPR-compliant. <a href=":url" lang="fr">Learn more</a>', [
          ':url' => 'https://eulerian.wiki/doku.php?id=fr:modules:collect:gdpr:tarteaucitron',
        ]),
    ];

    return $elements;
  }

}
